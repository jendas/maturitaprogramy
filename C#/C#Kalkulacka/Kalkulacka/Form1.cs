﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        void kalkuluj()
        {
            int cislo1, cislo2;
            bool parsed1 = Int32.TryParse(textBox1.Text, out cislo1);
            bool parsed2 = Int32.TryParse(textBox2.Text, out cislo2);
            int selectedIndex = comboBox1.SelectedIndex;
            

            if (parsed1 & parsed2)
            {
                switch(selectedIndex) {
                    case 0:
                Vysledek.Text = Convert.ToString(cislo1 + cislo2);
                return;
                    case 1:
                Vysledek.Text = Convert.ToString(cislo1 - cislo2);
                return;
                    case 2:
                Vysledek.Text = Convert.ToString(cislo1 * cislo2);
                return;
                    case 3:
                if (cislo2 > 0) Vysledek.Text = String.Format("{0:0.00}", ((double)cislo1 / (double)cislo2));
                return;
                    default:
                Vysledek.Text = "";
                return;
                      
            }
            }
            else
            {
                Vysledek.Text = "";
            }
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            kalkuluj();
        }

        private void secti_Click(object sender, EventArgs e)
        {
            int cislo1, cislo2;
            cislo1 = Convert.ToInt32(textBox1.Text);
            cislo2 = Convert.ToInt32(textBox2.Text);
            Vysledek.Text = Convert.ToString(cislo1 + cislo2);

        }

        private void Vysledek_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            kalkuluj();
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            kalkuluj();
        }
    }
}
