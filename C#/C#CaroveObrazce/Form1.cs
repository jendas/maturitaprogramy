﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Čarové_obrazce
{
    public partial class oknoProgramu : Form
    {
        public oknoProgramu()
        {
            InitializeComponent();
        }

        void kresli(object sender, PaintEventArgs e)
        {

        }
        private void oknoProgramu_Paint(object sender, PaintEventArgs e)
        {
            // Nejprve získáme objekt kreslící plochy
            Graphics Canvas;
            Canvas = e.Graphics;

            float height = Form.ActiveForm.Size.Height;
            float width = Form.ActiveForm.Size.Width;
            label1.Text = Convert.ToString( height);
            Canvas.FillRectangle(Brushes.Green, 0, 0, height , width);
            // Nakreslíme černou čáru z bodu [0, 0] do bodu [200, 100]

            // Význam parametrů: (pero, xPoč, yPoč, xKonc, yKonc) 
            Canvas.DrawLine(Pens.Black, 0, 0, 200, 100);
            //Canvas.DrawRectangle(Pen.Yellow, 0, 0, 100, 100);
            
            //Canvas.FillRectangle(Brushes.Green, 0, 0, 100, 100);

            // Ještě jednu čáru - vodorovnou z [0, 100] do [200, 100]
            Canvas.DrawLine(Pens.Green, 0, 100, 200, 100);

            // Nakreslíme modrý obdélník v bodě [200, 100]
            //   o šířce 50 pixelů a výšce 100 pixelů
            // Význam parametrů: 
            //   (pero, xLevýHorní, yLevýHorní, šířka, výška)
            Canvas.DrawRectangle(Pens.Blue, 200, 100, 50, 100);

            // Nakreslíme čokoládovou elipsu umístěnou uvnitř
            //   obdélníka, který začíná v bodě [200, 100] a
            //   je 50 pixelů široký a 100 pixelů vysoký
            // Význam parametrů je stejný jako při kreslení obdélníka
            Canvas.DrawEllipse(Pens.Chocolate, 200, 100, 50, 100);
        }

        private void oknoProgramu_Load(object sender, EventArgs e)
        {

            
        }

        private void oknoProgramu_Resize(object sender, EventArgs e)
        {
            this.Update();
            label1.Text = "Resizing";
           // Canvas.FillRectangle(Brushes.Green, 0, 0, SSize.Height / 2, SSize.Width / 2);
        }

        private void oknoProgramu_ResizeEnd(object sender, EventArgs e)
        {
            this.Update();
            label1.Text ="Resize end";
        }
    }
}

