﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Ahoj_Click(object sender, EventArgs e)
        {
            textBox1.Text = "Ahoj";
        }

        private void Odblokuj_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
        }

        private void Zablokuj_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
        }

        private void Smaz_Click(object sender, EventArgs e)
        {
            textBox1.Text = null;
        }

        private void Cervena_barva_Click(object sender, EventArgs e)
        {
            textBox1.ForeColor = Color.Red;
        }

       

        
    }
}
