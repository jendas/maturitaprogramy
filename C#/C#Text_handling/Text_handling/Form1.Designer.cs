﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ahoj = new System.Windows.Forms.Button();
            this.Cervena_barva = new System.Windows.Forms.Button();
            this.Odblokuj = new System.Windows.Forms.Button();
            this.Zablokuj = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Smaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ahoj
            // 
            this.Ahoj.Location = new System.Drawing.Point(26, 76);
            this.Ahoj.Name = "Ahoj";
            this.Ahoj.Size = new System.Drawing.Size(75, 23);
            this.Ahoj.TabIndex = 0;
            this.Ahoj.Text = "Ahoj";
            this.Ahoj.UseVisualStyleBackColor = true;
            this.Ahoj.Click += new System.EventHandler(this.Ahoj_Click);
            // 
            // Cervena_barva
            // 
            this.Cervena_barva.Location = new System.Drawing.Point(26, 119);
            this.Cervena_barva.Name = "Cervena_barva";
            this.Cervena_barva.Size = new System.Drawing.Size(75, 23);
            this.Cervena_barva.TabIndex = 1;
            this.Cervena_barva.Text = "Cervena_barva";
            this.Cervena_barva.UseVisualStyleBackColor = true;
            this.Cervena_barva.Click += new System.EventHandler(this.Cervena_barva_Click);
            // 
            // Odblokuj
            // 
            this.Odblokuj.Location = new System.Drawing.Point(154, 76);
            this.Odblokuj.Name = "Odblokuj";
            this.Odblokuj.Size = new System.Drawing.Size(75, 23);
            this.Odblokuj.TabIndex = 2;
            this.Odblokuj.Text = "Odblokuj";
            this.Odblokuj.UseVisualStyleBackColor = true;
            this.Odblokuj.Click += new System.EventHandler(this.Odblokuj_Click);
            // 
            // Zablokuj
            // 
            this.Zablokuj.Location = new System.Drawing.Point(154, 119);
            this.Zablokuj.Name = "Zablokuj";
            this.Zablokuj.Size = new System.Drawing.Size(75, 23);
            this.Zablokuj.TabIndex = 3;
            this.Zablokuj.Text = "Zablokuj";
            this.Zablokuj.UseVisualStyleBackColor = true;
            this.Zablokuj.Click += new System.EventHandler(this.Zablokuj_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(26, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(203, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Smaz
            // 
            this.Smaz.Location = new System.Drawing.Point(26, 166);
            this.Smaz.Name = "Smaz";
            this.Smaz.Size = new System.Drawing.Size(75, 23);
            this.Smaz.TabIndex = 5;
            this.Smaz.Text = "Smaz";
            this.Smaz.UseVisualStyleBackColor = true;
            this.Smaz.Click += new System.EventHandler(this.Smaz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 210);
            this.Controls.Add(this.Smaz);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Zablokuj);
            this.Controls.Add(this.Odblokuj);
            this.Controls.Add(this.Cervena_barva);
            this.Controls.Add(this.Ahoj);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Ahoj;
        private System.Windows.Forms.Button Cervena_barva;
        private System.Windows.Forms.Button Odblokuj;
        private System.Windows.Forms.Button Zablokuj;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Smaz;
    }
}

