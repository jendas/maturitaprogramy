program Faktorial;

var Vysledek:LongInt;
var Faktorial_cisla:integer;
var Nasobitel:integer;
  {
  Faktorial se značí n! a vypočítá se jako součin všech
  kladných celých čísel menších než n
  př. 5! = 5*4*3*2*1 = 120
   }
begin;
writeln ('Zadej kladné číslo, od kterého chceš zjistit faktoriál');
readln(Faktorial_cisla);

Vysledek := 1;//faktorial 0 je 1
//výpočet faktoriálu jako součin čísel od Faktoriál čísla po nulu
//for Nasobitel:= Faktorial_cisla downto 1 do Vysledek:=Vysledek*Nasobitel;

//výpočet faktoriálu jako součin čísel od 1 po Faktorial cisla pomoci repeat until
Nasobitel:= 1;
repeat
  Vysledek := Nasobitel*vysledek;
  Nasobitel := Nasobitel + 1;
until (Nasobitel > Faktorial_cisla);

//vypis vysledek na obrazovku
writeln('Faktorial ',Faktorial_cisla, ' je ', Vysledek);
readln;
end.





