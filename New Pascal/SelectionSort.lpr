program SelectionSort;

{      Př. seřazování
(3 2 8 7 6) // zadání pole, řaďmě od největšího k nejmenšímu
(3 2 8 7 6) // nejvyšší číslo je 8, prohoďme ho tedy s číslem 3 na indexu 0
(8 2 3 7 6) // nejvyšší číslo je 7, prohoďme ho tedy s číslem 2 na indexu 1
(8 7 3 2 6) // nejvyšší číslo je 6, prohoďme ho tedy s číslem 3 na indexu 2
(8 7 6 2 3) // nejvyšší číslo je 3, prohoďme ho tedy s číslem 2 na indexu 3
(8 7 6 3 2) // seřazeno
}



procedure SelectionSort(var A: array of Integer);
var
  temp: Integer;
  i, j, min_index: Integer;

begin
  //od 1. prvku do předposledního (poslední bude už seřazen)
  for i:= Low(A) to High(A) - 1 do begin
    min_index:= i;

    //najdi minimum u ostatních prvků zprava
    for j:= i + 1 to High(A) do
      if A[j] < A[min_index] then min_index:= j;

    //prohod prvky
    temp:= A[min_index];
    A[min_index]:= A[i];
    A[i]:= temp;
  end;
end;

var
  PoleCisel: array of Integer;
  I: Integer;

begin

  SetLength(PoleCisel, 16);
  //náhodná čísla
  for I:= Low(PoleCisel) to High(PoleCisel) do
    PoleCisel[I]:= Random(100);

  //vypiš čísla
  for I:= Low(PoleCisel) to High(PoleCisel) do
    Write(PoleCisel[I]:3);
  Writeln;

  //seřaď čísla
  SelectionSort(PoleCisel);
  for I:= Low(PoleCisel) to High(PoleCisel) do
    Write(PoleCisel[I]:3);
  Writeln;
  Readln;
end.
