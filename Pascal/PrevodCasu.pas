program PrevodCasu;

var sekundy: longInt;
var minuty, hodiny, dny: longInt;
var zbytek_sekund, zbytek_minut, zbytek_hodin : longInt;

begin;

write('zadej cas v sekundach: ');
readln(sekundy);

//převedení sekund do počtu minut, hodin a dnů
minuty := sekundy div 60;
hodiny := sekundy div (60*60);
dny := sekundy div (60*60*24);

//modulo je zbytek po celočíselném dělení

zbytek_sekund := sekundy mod 60;
zbytek_minut  := minuty mod 60;
zbytek_hodin  := hodiny mod 24;



writeln;
writeln('Cas v minutach je: ', minuty, ' minut a ', zbytek_sekund, ' sekund');
writeln('Cas v hodinach je: ', hodiny, ' hodin, ', zbytek_minut, ' minut a ', zbytek_sekund, ' sekund.');
writeln('Cas ve dnech je: ', dny, ' dnu, ', zbytek_hodin, ' hodin, ', zbytek_minut, ' minut a ', zbytek_sekund, ' sekund.');


writeln;
write('Zadej počet dnů: ');
readln(dny);
write('Zadej počet hodin: ');
readln(hodiny);
write('Zadej počet minut: ');
readln(minuty);
write('Zadej počet sekund: ');
readln(sekundy);

writeln('Převedeno na sekundy: ', sekundy+ 60*minuty + 60*60*hodiny + 60*60*24*dny );

readln;
end.
