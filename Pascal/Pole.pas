program Pole;

var i, cislo, max, pocet_nul:integer;
var cisla: array[0..4] of integer;

begin
writeln('Program na uložení čísel do pole (array)');
max := -maxint;
//zapisování čísel
for i:=0 to 4 do
begin
write('Zadej číslo: ');
readln (cislo);

if cislo > max then max:= cislo;
if cislo = 0 then pocet_nul := pocet_nul +1;
cisla[i] := cislo;

end;

//vypsání čísel
for i:=0 to 4 do
begin
writeln ('Jako ', i + 1 ,'. jsi zadal číslo: ', cisla[i]);
end;
writeln;
writeln('Největší z nich je číslo ', max);
writeln('Počet nul je: ',pocet_nul);

readln;
end.
