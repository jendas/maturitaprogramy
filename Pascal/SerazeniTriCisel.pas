program SerazeniTriCisel;

var prvni, druhe, treti:integer;
var pomocne: integer;

begin
writeln('Program na seřazení tří čísel');
write('Zadej první číslo: ');
readln(prvni);
write('Zadej druhé číslo: ');
readln(druhe);
write('Zadej třetí číslo: ');
readln(treti);

//pokud je první větší než druhé, přehodím je

if prvni < druhe then
   begin
        pomocne := prvni;
        prvni  := druhe;
        druhe := pomocne;
   end;

//pokud je druhé větší než třetí, přehodím je
if druhe < treti then
       begin
       pomocne := druhe;
       druhe := treti;
       treti := pomocne;
       end;

//Kontrola jestli náhodou není přehozené třetí číslo větší jak druhé
{
Př. čísla: 10 20 30
po prvním přehozením: 20 10 30
po druhém přehozením: 20 30 10
po třetím přehozením: 30 20 10 - až zde jsou seřazená
}
if prvni < druhe then
   begin
        pomocne := prvni;
        prvni  := druhe;
        druhe := pomocne;
   end;

 Writeln('Sezařená čísla jsou v sestupném pořadí');
 Writeln('První: ',prvni);
 Writeln('Druhé: ',druhe);
 Writeln('Třetí; ',treti);

readln;
end.

