program ScitaniCisel;

var cislo, soucet:integer;

begin

writeln('Program sloužící na součet čísel');

writeln('Pomocí smyčky while do');
//smyčka while do
cislo:=1;
while (cislo<>0) do begin
write('Cislo: ');
readln(cislo);
soucet := soucet + cislo;
end;

writeln ('Součet je:', soucet);

writeln('Pomocí smyčky repeat until');

soucet:=0; //vynulování součtu
//smyčka repeat until
repeat
  begin
  write('Cislo: ');
  readln(cislo);
  soucet := soucet + cislo;
  end;
until cislo = 0;

writeln ('Součet je:', soucet);
readln;
end.
