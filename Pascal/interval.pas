program interval;

label zacatek;
label konec;
label dotaz_opakovat;
  //label neboli štítek označí část kódu, ke které se jde pomocí příkazu goto vrátit
var znak: char;
var dolni_hranice, horni_hranice, testovane_cislo:integer;

begin;
zacatek:  //označení štítkem (labelem) místo, kde se chceme vrátit
writeln ('Zadej dolní hranici ');
readln (dolni_hranice);

writeln ('Zadej horní hranici. Pokud bude menší nebo rovno ', dolni_hranice,', tak tě vrátí program na začátek. ');
readln (horni_hranice);

if dolni_hranice >= horni_hranice then goto zacatek;

writeln ('zadej císlo, u kterého chces zjistit, jestli patří do intervalu');
readln (testovane_cislo);


if (horni_hranice>=testovane_cislo) and (testovane_cislo>=dolni_hranice)
then writeln ('Číslo ', testovane_cislo,' patří do uzavřeného intervalu čísel ', dolni_hranice, ' a ', horni_hranice)
else writeln ('Číslo ', testovane_cislo,' nepatří do intervalu čísel ', dolni_hranice, ' a ', horni_hranice);

dotaz_opakovat:
writeln ('Znovu? a/n');
readln (znak);

if znak='a' then
goto zacatek;
if znak='n' then
goto konec;

writeln ('Řekl jsem a/n a ne nějaké ', znak);
goto dotaz_opakovat;

konec:
writeln('Konec');

readln;
end.
