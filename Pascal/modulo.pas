program modulo;

var cislo: integer;
var delitel: integer;

begin;

writeln ('Zadej číslo');
readln (cislo);
writeln ('Zadej dělitele');
readln (delitel);

if cislo mod delitel = 0
then writeln ('Číslo ', cislo, ' je dělitelné ', delitel, '.')
else
  begin
  writeln ('Číslo ', cislo, ' není dělitelné ', delitel, '.');
  writeln ('Zbytek je ', cislo mod delitel);
  end;

readln;
end.
