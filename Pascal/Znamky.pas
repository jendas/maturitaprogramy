program Znamky;


var i, znamka, pocetjednicek:integer;
var znamky:array [0..9] of integer;
var soucet_znamek:integer;
var prumer:real;

begin;

randomize;

//zapisování známek
for i:=0 to 9 do
begin

//vygenerování náhodného čísla jako známky
// + 1 je aby známka byla od 1-5, random totiž dává čísla od 0 po zadané číslo
znamka := random(4) + 1;

znamky[i] := znamka;//zapsání do pole
soucet_znamek := soucet_znamek + znamka;
//počítání jedniček
if znamka = 1 then pocetjednicek := pocetjednicek + 1;
end;

//výpis známek
write('Zadane znamky jsou: ');
for i:=0 to 8 do begin
write(znamky[i],', ');
end;
writeln(znamky[9]);

prumer:=soucet_znamek / 10;
writeln('Průměr je:          ', prumer:0:2);
writeln('Počet jedniček je:  ', pocetjednicek);

readln;
end.
