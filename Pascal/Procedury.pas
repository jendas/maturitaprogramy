
// Funkce na výpočet mocniny
function mocnina(x:integer): integer;
 begin
 mocnina:= x*x;
 end;

//Funkce Max, která najde v poli největší číslo:

function max(var Cisla: array of integer):integer;
var i: integer;
begin
max := -maxint;
for i := 0 to length(Cisla) do
begin
if Cisla[i] > max then
max:=Cisla[i];
end;
end;


//Procedura prohod, která prohodí čísla x a y:
//Klíčové slovo var před parametry procedury způsobí, že se procedura provede s parametry hlavního programu (tzv. parametr volaný odkazem).

procedure prohod(var x,y: integer);
 var p:integer;
 begin
     p:=y;
     y:=x;
     x:=p;
 end;

//Proceduru prohod tak, aby prohodila čísla x a y, pokud je x>y:

procedure prohodVetsiMensi(var x,y: integer);
 var p:integer;
 begin
   if x>y then
    begin
     p:=y;
     y:=x;
     x:=p;
    end;
 end;

var a,b,i: integer;
Var Pole: array[0..9] of integer;
begin;

    {
Writeln('Prohození 2 čísel.');
a := 5;
b := 203;
prohod(a,b);
Writeln('Prohozená čísla: A = ',a,', B = ',b );
     }

      {
Randomize;
Write('Čísla pole: ');
for i:=0 to 8 do
     begin
     pole[i]:=random(10);
     write (pole[i], ', ');
     end;
  pole[9]:=random(10);
  writeln(pole[i]);

  writeln;
  write('Největší číslo v poli je ', max(pole), '.');
      }


Readln;
end.
