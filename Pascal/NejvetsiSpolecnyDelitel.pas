program NejvetsiSpolecnyDelitel;

var prvni, druhe, pocet_kroku:LongInt;

begin;

 {Největším společným dělitelem (nsd, greatest common divisor (zkratka: gcd))
 je číslo, které dělí každé z těchto čísel, a je maximální. }

writeln ('Program na vypočtení největšího společného dělitele dvou čísel');
write ('První číslo: ');
readln(prvni);
write('Druhé číslo: ');
readln(druhe);


repeat
begin
if prvni>druhe then prvni:=prvni-druhe;
if prvni<druhe then druhe:=druhe-prvni;
pocet_kroku:=pocet_kroku+1;
end;
until prvni=druhe;

writeln ('NSD je ', prvni);
writeln ('Výsledek se vypočítal v ', pocet_kroku, ' krocích.');




readln;
end.
